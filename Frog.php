<?php
class Frog 
{
    public $lompat = "hop hop" ;
    public $cold_blooded = "true";    
    public function jump()
    {
        print $this ->lompat; 
    }

    function __construct($name)
    { $this->name = $name; }


    function setLegs($legs) 
    { $this->legs = $legs; }

    function getName()
    { return $this->name; }

    function getLegs()
    { return $this->legs; }

    function getBlood()
    { return $this->cold_blooded;} 
}
?>
