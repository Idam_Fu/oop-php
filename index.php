<?php

require 'Animal.php';
require 'Ape.php';
require 'Frog.php';


$sheep = new Animal("shaun");
echo "Name of Animal is : ".$sheep->getName(); //"shaun"
echo "<br>";
$sheep ->setLegs(2);
echo "Total legs are : ".$sheep->getLegs();// 2
echo"<br>"; 
echo "Cold Blooded is : ".$sheep->getBlood(); // false
echo "<br> <br>"; 
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$sungokong = new Ape("kera sakti");
echo "Name of Animal is : ".$sungokong->getName();
echo "<br>";
$sungokong ->setLegs(2);
echo "Total legs are : ".$sungokong->getLegs();// 2
echo"<br>"; 
echo "Cold Blooded is : ".$sungokong->getBlood(); // false
echo "<br>";
echo "Sound when Yell : ";
echo $sungokong->yell();// "Auooo"
echo "<br> <br>";

$kodok = new Frog("buduk");
echo "Name of Animal is : ".$kodok->getName();
echo "<br>";
$kodok ->setLegs(4);
echo "Total legs are : ".$kodok->getLegs();// 2
echo"<br>"; 
echo "Cold Blooded is : ".$kodok->getBlood(); // false
echo "<br>";
echo "Sound when Jump : ";
echo $kodok->jump();// "hop hop"

?>
